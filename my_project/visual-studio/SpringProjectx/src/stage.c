/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "common.h"
#include "time.h"

static void logic(void);
static void draw(void);
static void drawHud(void);

void initStage(void)
{
	app.delegate.logic = logic;
	app.delegate.draw = draw;

	memset(&stage, 0, sizeof(Stage));

	stage.entityTail = &stage.entityHead;

	initEntities();

	initPlayer();

	initMap();
}

static void logic(void)
{
	doPlayer();

	doEntities();

	doCamera();
}

static void draw(void)
{
	SDL_SetRenderDrawColor(app.renderer, 128, 255, 255, 255);
	SDL_RenderFillRect(app.renderer, NULL);

	drawMap();

	drawEntities();

	drawHud();
}

static void drawHud(void)
{
	SDL_Rect r;

	r.x = 0;
	r.y = 0;
	r.w = SCREEN_WIDTH;
	r.h = 35;

	SDL_Rect z;

	z.x = SCREEN_WIDTH-785;
	z.y = 295;
	z.w = 250;
	z.h = 35;

	SDL_Rect x;

	x.x = SCREEN_WIDTH - 237;
	x.y = 75;
	x.w = 225;
	x.h = 35;

	double timer_count = 0.0;     
	clock_t begin = clock();          
	timer_count += (double)(begin) / CLOCKS_PER_SEC;

	double completed = 0.0;
	clock_t complTime = clock();
	completed += (double)(complTime) / CLOCKS_PER_SEC;

	// completed in

	int completed_time = 0.0;
	completed_time += completed;

	// timer

	int timer = 250 - (int)timer_count; //time based score       

	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(app.renderer, 0, 0, 0, 196);
	SDL_RenderFillRect(app.renderer, &r);
	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);

	// if timer == 0 {exit} 

	if (timer == 0) {
		exit(1);
	}
  
	drawText(190, 5, 255, 255, 255, TEXT_RIGHT, "TIME LIMIT: %d", timer);
	if (player->health > 0) {

		SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_BLEND);
		SDL_SetRenderDrawColor(app.renderer, 0, 0, 0, 196);
		SDL_RenderFillRect(app.renderer, &x);
		SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);

		drawText(SCREEN_WIDTH - 25, 80, 255, 255, 255, TEXT_RIGHT, "HEALTH: %d", player->health);
	}
	else {

		SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_BLEND);
		SDL_SetRenderDrawColor(app.renderer, 0, 0, 0, 196);
		SDL_RenderFillRect(app.renderer, &z);
		SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);

		drawText(SCREEN_WIDTH - 550, 300, 255, 0, 0, TEXT_RIGHT, "YOU ARE DEAD");
	}
	drawText(SCREEN_WIDTH - 450, 5, 245, 108, 66, TEXT_RIGHT, "COMPLETED IN: %d SECONDS", completed_time);
	drawText(SCREEN_WIDTH - 5, 5, 255, 255, 255, TEXT_RIGHT, "PIZZA %d/%d", stage.pizzaFound, stage.pizzaTotal);

	SDL_Rect b;

	b.x = SCREEN_WIDTH - 350;
	b.y = SCREEN_HEIGHT - 60;
	b.w = 340;
	b.h = 50;

	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(app.renderer, 0, 0, 0, 700);
	SDL_RenderFillRect(app.renderer, &b);
	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);

	drawText(SCREEN_WIDTH - 25, SCREEN_HEIGHT - 50, 255, 255, 255, TEXT_RIGHT, "PRESS ESC TO EXIT");

	SDL_Rect c;

	c.x = 10;
	c.y = SCREEN_HEIGHT - 60;
	c.w = 425;
	c.h = 50;

	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(app.renderer, 0, 0, 0, 700);
	SDL_RenderFillRect(app.renderer, &c);
	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);

	drawText(25, SCREEN_HEIGHT - 50, 255, 255, 255, TEXT_LEFT, "PRESS X FOR SUPER JUMP");

	SDL_Rect d;

	d.x = SCREEN_WIDTH - 820;
	d.y = SCREEN_HEIGHT - 60;
	d.w = 450;
	d.h = 50;

	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(app.renderer, 0, 0, 0, 700);
	SDL_RenderFillRect(app.renderer, &d);
	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);

	drawText(SCREEN_WIDTH - 800, SCREEN_HEIGHT - 50, 255, 255, 255, TEXT_LEFT, "PRESS Z TO DASH FORWARD");

}

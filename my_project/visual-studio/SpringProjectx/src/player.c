/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "common.h"
#include "stdbool.h"
#include "time.h"
#include "Windows.h"

bool jumped = false;

static SDL_Texture *pete[2];

void cooldown(int seconds)
{
	clock_t start = clock();
	clock_t period = seconds * CLOCKS_PER_SEC;
	clock_t elapsed;

	do {
		elapsed = clock() - start;
	} while (elapsed < period);
}

void initPlayer(void)
{
	player = malloc(sizeof(Entity));
	memset(player, 0, sizeof(Entity));
	stage.entityTail->next = player;
	stage.entityTail = player;

	player->health = 100;

	pete[0] = loadTexture("gfx/pete01.png");
	pete[1] = loadTexture("gfx/pete02.png");

	player->texture = pete[0];

	SDL_QueryTexture(player->texture, NULL, NULL, &player->w, &player->h);
}

void doPlayer(void)
{
	player->dx = 0;

	if (app.keyboard[SDL_SCANCODE_A])
	{
		player->dx = -PLAYER_MOVE_SPEED;

		player->texture = pete[1];
	}

	if (app.keyboard[SDL_SCANCODE_D])
	{
		player->dx = PLAYER_MOVE_SPEED;

		player->texture = pete[0];
	}

	if (app.keyboard[SDL_SCANCODE_Z])
	{
		bool dashed = false;
		if (dashed == false) {
			player->dx = PLAYER_MOVE_SPEED * 2;
			dashed = true;
		}
		else {
			Sleep(10000);
			dashed = false;
		}
	}

	if (app.keyboard[SDL_SCANCODE_SPACE] && player->isOnGround)
	{
		player->riding = NULL;

		player->dy = -20;

		playSound(SND_JUMP, CH_PLAYER);
	}

	if (app.keyboard[SDL_SCANCODE_I])
	{
		player->x = player->y = 0;

		app.keyboard[SDL_SCANCODE_SPACE] = 0;
	}

	if (app.keyboard[SDL_SCANCODE_ESCAPE])
	{
		exit(1);
	}

	if (app.keyboard[SDL_SCANCODE_X] && player->isOnGround) {

		player->riding = NULL;
		if (jumped == false) {
			player->dy = -30;
			jumped = true;
		}
		else {
			Sleep(10000);
			jumped = false;
		}

		playSound(SND_JUMP, CH_PLAYER);
	}
}
